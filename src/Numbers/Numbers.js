import React from "react";

const Numbers = props => {
    return (
        <div className="numbers">
            <ul>
                <li className="list">{props.firstNumber}</li>
                <li className="list">{props.secondNumber}</li>
                <li className="list">{props.thirdNumber}</li>
                <li className="list">{props.forthNumber}</li>
                <li className="list">{props.fifthNumber}</li>
            </ul>
        </div>
    )
}

export default Numbers;