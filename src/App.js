import React from "react";
import './App.css';
import Numbers from './Numbers/Numbers'


class App extends React.Component {
    state = {
        numbers: []
    };

    randomInteger = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    createNumbers = () => {
        let randNumbers = [];

        while (randNumbers.length < 5) {
            const newNum = this.randomInteger(5, 36);

            if (randNumbers.includes(newNum)) {
                continue;
            } else {
                randNumbers.push(newNum);
            }
        }
        let state = {...this.state};
        state.numbers = randNumbers;
        this.setState(state);
        randNumbers.sort((a, b) => {
            return a - b
        })
    };

    render() {
        return (
            <div className="App">
                <div className="border">
                    <div>
                        <button className="btn" onClick={this.createNumbers}>New numbers</button>
                    </div>
                    <Numbers firstNumber={this.state.numbers[0]}
                             secondNumber={this.state.numbers[1]}
                             thirdNumber={this.state.numbers[2]}
                             forthNumber={this.state.numbers[3]}
                             fifthNumber={this.state.numbers[4]}
                    ></Numbers>
                </div>
            </div>
        )
    }
}

export default App;
